### Как пользоваться линтерами в проекте

#### Установка pre-commit и ruff
1.1 pip install pre-commit \
1.2 pre-commit install \
1.3 pip install ruff

#### Запуск линтеров вручную
2.1 ruff chech \
или \
2.2 pre-commit run --all-files

#### Автоматическое исправление ошибок линтером
3.1 ruff chech --fix
